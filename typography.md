---
layout: default
---

# Web Developer

## Hunt Valley, MD

Immediate fulltime, direct hire opportunity for a Web Developer with our client in Hunt Valley, MD.

### Overview

Make your mark in Broadcasting and Digital Media. Our client is dedicated to making themselves a communications powerhouse! They are the largest and most diversified television broadcasting company in the country today. Are you ready for a highly involved, hands-on leadership position? We are at an exciting point in our growth and are seeking a top performer to lead the quality assurance effort for our software production. As the new SQA Manager, you will provide strategic thinking, planning.

Spaceflights cannot be stopped. This is not the work of any one man or even a group of men. It is a historical process which mankind is carrying out in accordance with the natural laws of human development.

To be the first to enter the cosmos, to engage, single-handed, in an unprecedented duel with nature—could one dream of anything more?

For those who have seen the Earth from space, and for the hundreds and perhaps thousands more who will, the experience most certainly changes your perspective. The things that we share in our world are far more valuable than those which divide us.

### Hypertext Markup Language (HTML)

HTML is the unifying language of the World Wide Web. Using just the simple tags it contains, the human race has created an astoundingly diverse network of hyperlinked documents, from Amazon, eBay, and Wikipedia, to personal blogs and websites dedicated to cats that look like Hitler.

### Cascading Style Sheets (CSS)

CSS isn’t easy. Combining a very permissive (somewhat broken) syntax with constantly evolving features and rendering inconsistencies makes CSS not that easy at all. Yes, the syntax is simple, but a simple syntax doesn’t make an easy language.

### JavaScript (JS)

I am not content, nor should you be, at stopping once something just works, and not really knowing why. I gently challenge you to journey down that bumpy "road less traveled" and embrace all that JavaScript is and can do. With that knowledge, no technique, no framework, no popular buzzword acronym of the week, will be beyond your understanding.

### Responsibilities

* Writing production code for agency and internal clients.
* Leading WooCommerce implementations.
* Documenting new systems as well as current systems that may need treatment
* Providing guidance to designers on the workings of reusable components.
* Learning and extending the capabilities of our WordPress framework.
* DNS configuration and participating in the configuration planning.
* Participate in requirement analysis and provide input towards creating client contracts.
* Creating custom, web based apps with JavaScript and PHP


