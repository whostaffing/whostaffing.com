var CSI = {
	Util: {
		scrollToElement: function (elId) {
			// need to scroll by a bit less for tablet/mobile layouts
			var adjustment = $(window).width() < 992 ? 60 : 0;
			var scrollTarget = $('#' + elId).offset().top - adjustment;

			$('html, body').animate({
				scrollTop: scrollTarget
			}, 700);
		}
	}
};


// testimonial class
// ----------------------------------------------------------------------------
function Testimonial() {
	var self = this;

	this.$container = $('.csi-testimonial-container').first();
	this.$testimonials = this.$container.find('.csi-testimonial-content');
	this.testimonialCount = this.$testimonials.length;
	this.currentIndex = 0;

	this.getPrevIndex = function() {
		return (self.currentIndex == 0) ? (this.testimonialCount - 1) : self.currentIndex - 1;
	};

	this.getNextIndex = function() {
		return (self.currentIndex == self.testimonialCount - 1) ? 0 : self.currentIndex + 1;
	};

	this.showTestimonial = function(index) {
		self.$testimonials.eq(self.currentIndex).fadeOut(400, function() {
			self.$testimonials.eq(index).fadeIn(400);
			self.currentIndex = index;
		});
	};

	// set the left-nav button click handler
	this.$container.find('.csi-testimonial-navleft a').on('click', function(e) {
		e.preventDefault();

		var prevIndex = self.getPrevIndex();
		self.showTestimonial(prevIndex);
	});

	// set the right-nav button click handler
	this.$container.find('.csi-testimonial-navright a').on('click', function(e) {
		e.preventDefault();

		var nextIndex = self.getNextIndex();
		self.showTestimonial(nextIndex);
	});
}

// yearbook class
// ----------------------------------------------------------------------------
function Yearbook() {
	var self = this;

	this.$portraits = $('.csi-yearbook .csi-yearbook-portrait');
	this.$pages = $('.csi-yearbook-page-container .csi-yearbook-page');
	this.isOpen = false;
	this.currentPortraitIndex = -1;

	this.openYearbook = function(index) {
		// highlight the portrait
		self.$portraits.addClass('dim');
		self.$portraits.eq(index).removeClass('dim');

		// display the page
		self.$pages.hide();
		self.$pages.eq(index).show();

		self.isOpen = true;
		self.currentPortraitIndex = index;
	};

	this.closeYearbook = function() {
		// hide the pages
		self.$pages.hide();

		// undim all the portraits
		self.$portraits.removeClass('dim');

		self.isOpen = false;
		self.currentPortraitIndex = -1;
	};

	// set hover state for yearbook portraits
	this.$portraits.hover(function() {
		$(this).addClass('hover');
	}, function () {
		$(this).removeClass('hover');
	});

	// set click handler for portraits
	this.$portraits.on('click', function(e) {
		e.preventDefault();

		var portraitIndex = parseInt($(this).data('portraitindex'));

		if (self.isOpen && self.currentPortraitIndex == portraitIndex) {
			self.closeYearbook();
		}
		else {
			self.openYearbook(portraitIndex);
		}
	});
}

// mobile yearbook class
// ----------------------------------------------------------------------------
function MobileYearbook() {
	var self = this;

	this.$portraits = $('.csi-yearbook-mobile .csi-yearbook-portrait');
	this.$pages = $('.csi-yearbook-mobile-page-container .csi-yearbook-page');
	this.portraitCount = this.$portraits.length;
	this.currentIndex = 0;

	this.getPrevIndex = function() {
		return (self.currentIndex == 0) ? (this.portraitCount - 1) : self.currentIndex - 1;
	};

	this.getNextIndex = function() {
		return (self.currentIndex == self.portraitCount - 1) ? 0 : self.currentIndex + 1;
	};

	this.showPage = function(index) {
		var portrait = self.$portraits[self.currentIndex];
		var page = self.$pages[self.currentIndex];
		
		$([portrait, page]).fadeOut(400, function() {
			var nextPortrait = self.$portraits[index];
			var nextPage = self.$pages[index];

			$([nextPortrait, nextPage]).fadeIn(400);
			self.currentIndex = index;
		});
	};

	// set the left-nav button click handler
	$('.csi-yearbook-mobile-navleft a').on('click', function(e) {
		e.preventDefault();

		var prevIndex = self.getPrevIndex();
		self.showPage(prevIndex);
	});

	// set the right-nav button click handler
	$('.csi-yearbook-mobile-navright a').on('click', function(e) {
		e.preventDefault();

		var nextIndex = self.getNextIndex();
		self.showPage(nextIndex);
	});
}
